package incrementor

const intMaxLen = int(^uint(0) >> 1)

// incrementor type is a demonstrate structure that iterate int values
// initialization incapsulated in constructor to protect initial values from editing
type incrementor struct {
	CurrentNumber int
	MaxNumber     int
}

// Public constructor that returns incrementor's pointer
// by default CurrentNumber = 0 and MaxNumber is a maximum length of int type
func NewIncrementor() *incrementor {
	return &incrementor{
		MaxNumber: intMaxLen,
	}
}

// Get the current number of incrementor
func (i *incrementor) GetNumber() int {
	return i.CurrentNumber
}

// IncrementNumber increases CurrentNumber by 1
// If CurrentNumber equals MaxNumber - set CurrentNumber to 0
func (i *incrementor) IncrementNumber() {
	i.CurrentNumber++
	if i.CurrentNumber >= i.MaxNumber {
		i.CurrentNumber = 0
	}
}

// SetMaximumValue sets the maximum possible value that can be returned by GetNumber
// Takes only positive values
func (i *incrementor) SetMaximumValue(maximumValue int) {
	if maximumValue >= 0 {
		i.MaxNumber = maximumValue
	}
}
