package incrementor

import (
	"testing"

	"github.com/go-playground/assert/v2"
)

func Test_incrementor_GetNumber(t *testing.T) {
	t.Run("Check initial values", func(t *testing.T) {
		expected := &incrementor{
			CurrentNumber: 0,
			MaxNumber:     intMaxLen,
		}

		inc := NewIncrementor()

		assert.Equal(t, expected, inc)
	})
}

func Test_incrementor_IncrementNumber(t *testing.T) {
	testTable := []struct {
		name     string
		inc      incrementor
		expected int
	}{
		{
			name: "Increment",
			inc: incrementor{
				CurrentNumber: 0,
				MaxNumber:     intMaxLen,
			},
			expected: 1,
		},
		{
			name: "Increment max",
			inc: incrementor{
				CurrentNumber: 1,
				MaxNumber:     1,
			},
			expected: 0,
		},
	}

	for _, tt := range testTable {
		t.Run(tt.name, func(t *testing.T) {
			tt.inc.IncrementNumber()
			assert.Equal(t, tt.expected, tt.inc.CurrentNumber)
		})
	}
}

func Test_incrementor_SetMaximumValue(t *testing.T) {
	testTable := []struct {
		name     string
		maxValue int
		expected int
	}{
		{
			name:     "positive",
			maxValue: 1,
			expected: 1,
		},
		{
			name:     "zero",
			maxValue: 0,
			expected: 0,
		},
		{
			name:     "negative",
			maxValue: -1,
			expected: intMaxLen,
		},
	}

	for _, tt := range testTable {
		t.Run(tt.name, func(t *testing.T) {
			inc := &incrementor{MaxNumber: intMaxLen}
			inc.SetMaximumValue(tt.maxValue)
			assert.Equal(t, tt.expected, inc.MaxNumber)
		})
	}
}
